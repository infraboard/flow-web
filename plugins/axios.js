export default function({ app, $axios, redirect }) {
    $axios.onRequest(config => {
        config.headers["Content-Type"] = "application/json;charset=UTF-8"
        config.headers["x-oauth-token"] = "R9bRjrYpAtunMM9VDUPhCIgL"
    });

    $axios.onResponse(response => {
        const res = response.data;
        if (res.code <= 0) {
            // -111:未登录;
            if (res.code === -111 || res.code === -200) {
                return res;
            } else if (res.code === -201 || res.code === -203) {
                return res;
            } else if (res.code === -206) {
                app.$dialog.toast({
                    mes: res.msg,
                    timeout: 1500,
                    icon: "error"
                });
                return res;
            } else {
                return res;
            }
        } else {
            return res;
        }
    });

    $axios.onError(error => {
        const code = parseInt(error.response && error.response.status);
    });
}
