export const GET_USER_INFO = (vm, params) => { 
  return vm.$axios.post(`/api/GetUserInfo`, params).then(res => res) 
}

export const GET_FILTER_OPTION_LIST = (vm, params) => { 
  return vm.$axios.post(`/api/GetFilterOptionList`, params).then(res => res) 
}

export const GET_MY_FLOW_LIST = (vm, params) => { 
  return vm.$axios.post(`/api/GetMyFlowList`, params).then(res => res) 
}

export const GET_ACCOUNT_LIST = (vm, params) => { 
  return vm.$axios.post(`/api/GetAccountList`, params).then(res => res) 
}


export const GET_BH_GROUP_LIST = (vm, params) => { 
  return vm.$axios.post(`/api/GetBhGroupList`, params).then(res => res) 
}
export const GET_BEHAVIOR_LIST = (vm, params) => { 
  return vm.$axios.post(`/api/GetBehaviorList`, params).then(res => res) 
}

export const GET_APP_ACCOUNT_LIST = (vm, params) => { 
  return vm.$axios.post(`/api/GetAppAccountList`, params).then(res => res) 
}

export const FLOW_SUBMIT = (vm, params) => { 
  return vm.$axios.post(`/api/SubmitPathFlowDef`, params, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).then(res => res) 
}

export const FLOW_QUERY = (vm, params) => { 
  return vm.$axios.post(`/api/GetPathFlowDetail`, params).then(res => res) 
}

export const ASYNC_SELECT = (vm, params) => { 
  return vm.$axios.post(`/api/AsyncSelect`, params, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).then(res => res) 
}

export const BATCH_LIST_VARIABLE = (vm, params) => { 
  return vm.$axios.post(`/api/BatchListVariable`, params, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).then(res => res) 
}