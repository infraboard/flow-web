import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment';
import filter from './filter';

Vue.use(Vuex)

const store = () => new Vuex.Store({
    state: {
        form: [],
        pipelineId: "",
        variables: [],
        actions: [],
        stepStatus: []
    },
    actions: {
        setStage({commit}, val){
            commit("SET_STAGE", val)
        },
        setPipelineId({commit}, val){
            commit("SET_PIPELINE_ID", val)
        },
        addStage({commit}, val){
            commit("ADD_STAGE", val)
        },
        delStage({commit}, val){
            commit("DEL_STAGE", val)
        },
        addParallel({commit}, obj){
            commit("ADD_PARALLEL", obj)
        },
        deleteCard({commit}, obj){
            commit("DELETE_CARD", obj)
        },
        addNode({commit}, obj){
            commit("ADD_NODE", obj)
        },
        setStageTitle({commit}, val){
            commit("SET_STAGE_TITLE", val)
        },
        setVariables({commit}, val){
            commit("SET_VARIABLES", val)
        },
        setActions({commit}, val){
            commit("SET_ACTIONS", val)
        },
        setStepStatus({commit}, val){
            commit("SET_STEP_STATUS", val)
        },
    },
    mutations: {
        SET_STAGE(state, val){
            state.form = val
        },
        SET_PIPELINE_ID(state, val){
            state.pipelineId = val
        },
        ADD_STAGE(state, val){
            state.form.push(val)
        },
        DEL_STAGE(state, val){
            state.form.forEach((v,i)=>{
                if(v.stageId === val){
                    state.form.splice(i, 1)
                }
            })
        },
        ADD_PARALLEL(state, val){
            state.form.forEach(v=>{
                if(v.stageId === val.stageId){
                    v.steps.forEach(v1=>{
                        if(v1.stepId === val.stepId){
                            v1.items.push({
                                type: 'card',
                                stepId: moment().valueOf() + Math.floor(Math.random()*100),
                                form: {}
                            })
                        }
                    })
                }
            })
        },
        DELETE_CARD(state, val){
            state.form.forEach(v=>{
                if(v.stageId === val.stageId){
                    v.steps.forEach((v1, i1)=>{
                        if(v1.type === "route"){
                            v1.items.forEach((v2,i2)=>{
                                if(v2.stepId === val.stepId){
                                    v1.items.splice(i2, 1)
                                }
                            })
                            if(v1.items.length === 1){
                                v.steps.splice(i1, 1)
                            }
                        }else{
                            if(v1.stepId === val.stepId){
                                v.steps.splice(i1, 1)
                            }
                        }
                    })
                }
            })
        },
        ADD_NODE(state, val){
            let tempCard = {
                type: 'card',
                stepId: moment().valueOf() + Math.floor(Math.random()*100),
                form: {
                    "name": "",
                    "action": "",
                    "with": [],
                    "ignore_failed": false,
                    "with_audit": false,
                    "webhooks": []
                }
            }
            let tempRoute = {
                type: 'route',
                stepId: moment().valueOf() + Math.floor(Math.random()*100),
                items: [
                    {
                        type: 'card',
                        stepId: moment().valueOf() + Math.floor(Math.random()*100),
                        form: {
                            "name": "",
                            "action": "",
                            "with": [],
                            "ignore_failed": false,
                            "with_audit": false,
                            "webhooks": []
                        }
                    },
                    {
                        type: 'card',
                        stepId: moment().valueOf() + Math.floor(Math.random()*100),
                        form: {
                            "name": "",
                            "action": "",
                            "with": [],
                            "ignore_failed": false,
                            "with_audit": false,
                            "webhooks": []
                        }
                    }
                ]
            }
            state.form.forEach(v=>{
                if(v.stageId === val.stageId){
                    if(val.stepId === undefined){
                        if(val.type == 'card'){
                            v.steps.unshift(tempCard)
                        }else{
                            v.steps.unshift(tempRoute)
                        }
                    }
                    v.steps.forEach((v1, i)=>{
                        if(v1.stepId === val.stepId){
                            if(val.type == 'card'){
                                v.steps.splice(i+1, 0, tempCard);
                            }else{
                                v.steps.splice(i+1, 0, tempRoute);
                            }
                        }
                    })
                }
            })
        },
        SET_STAGE_TITLE(state, val){
            state.form.forEach(v=>{
                if(v.stageId === val.stageId){
                    v.name = val.name
                }
            })
        },
        SET_VARIABLES(state, val){
            state.variables = val
        },
        SET_ACTIONS(state, val){
            state.actions = val
        },
        SET_STEP_STATUS(state, val){
            state.stepStatus = val
        },
    },
    getters: {
        "getStages": state => state.form,
        "getVariables": state => state.variables,
        "getActions": state => state.actions,
        "getStepStatus": state => state.stepStatus,
        "getPipelineId": state => state.pipelineId,
    },
    modules: {
        filter
    }
})

export default store
