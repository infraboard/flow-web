import moment from 'moment';
const state = ({
  optionsByFilter: [],
  optionsByApp: {
    trigger: [],
    action: []
  },
  optionsByAppForTrigger: [],
  optionsByAppForAction: [],
  optionsByBehavior: [],
  form: {
    flowId: '',
    name: '',
    description: '',
    items: [
      {
        type: 'card',
        stepId: moment().valueOf() + Math.floor(Math.random()*100),
        role: 1,
        groupId: '',
        group: {},
        behaviorId: '',
        behavior: {},
        form: {}
      },
    ]
  }
});
const getters = {
  queryForm: (state) => (val) => {
    return state.form;
  },
  queryFilter: (state) => (val) => {
    return state.optionsByFilter;
  },
  queryApp: (state) => (val) => {
    return state.optionsByApp;
  },
  queryBehavior: (state) => (val) => {
    return state.optionsByBehavior;
  }
};
const mutations = {
  SET_DATA_BY_FORM(state, val) {
    state.form = val;
  },
  SET_DATA_BY_FILTER(state, val) {
    state.optionsByFilter = val;
  },
  SET_DATA_BY_APP(state, val) {
    state.optionsByApp[val.role] = val.data;
  },
  SET_DATA_BY_APP_FOR_TRIGGER(state, val) {
    state.optionsByAppForTrigger = val;
  },
  SET_DATA_BY_APP_FOR_ACTION(state, val) {
    state.optionsByAppForAction = val;
  },
  SET_DATA_BY_BEHAVIOR(state, val) {
    state.optionsByBehavior = val;
  }
};
const actions = {
  async getInfo({state, commit}, val) {
    commit('SET_VALUE', val);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
