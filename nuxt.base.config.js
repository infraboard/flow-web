module.exports = {
  cache: true,
  loading: {
    color: '#409EFF',
    failedColor: 'red'
  },
  // 配置路由权鉴
  router: {
    // middleware: 'auth',
    base: '/flow/'
  },
  head: {
    title: '',
    meta: [
      {
        charset: 'utf-8'
      }, {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'
      }, {
        hid: 'description',
        name: 'description',
        content: 'Nuxt.js project'
      }
    ],
    // 配置icon
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/flow/favicon.ico'
      }
    ],
    script: [
      { src: 'https://cdn.staticfile.org/jquery/3.3.1/jquery.min.js' }
    ]
  },
  // 配置后台渲染或者spa模式
  mode: 'spa',
  // 引入nuxt/axios插件 proxy设置代理
  modules: [
    '@nuxtjs/axios',
    'cookie-universal-nuxt'
  ],
  css: [
    '~/assets/css/reset.css',
    '~/assets/css/iconfont.css',
    '~/assets/css/theme.css',
    'element-ui/lib/theme-chalk/index.css'
  ],
  build: {
    // 依赖的第三方模块
    vendor: ['axios']
  },
  // vue使用的第三方库 vue use之类的
  plugins: [
    { src: '~/plugins/axios', ssr: false },
    { src: '~/plugins/element-ui', ssr: false }
  ]
}
